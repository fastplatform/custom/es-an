# FaST Platform / es-an (Andalucia)

The module ```es-an``` is a module that inherits from the "[core](https://gitlab.com/fastplatform/core)" and implements customized services for Andalucia. It is also based on the reuse of some [additional](https://gitlab.com/fastplatform/addons) services.

This repository includes the [source code](services) and [a descriptive orchestration configuration](manifests) of the entire package.

## Services

### IACS

- [iacs/reafa](services/iacs/reafa)

### IDP

- [idp/authentication](services/idp/authentication)
