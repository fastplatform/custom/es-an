#!/bin/bash

export CI=${CI:-}
export CI_BASE_DOMAIN=${CI_BASE_DOMAIN:-${KUBE_INGRESS_BASE_DOMAIN:-ci-review-domain-not-set}}
export CI_COMMIT_REF_SLUG=${CI_COMMIT_REF_SLUG:-ci-commit-ref-slug-not-set}
export CI_ENVIRONMENT_DIR_SETTINGS=${CI_ENVIRONMENT_DIR_SETTINGS:-$(dirname $0)/envs/ephemeral-review}
export CI_ENVIRONMENT_INITIALIZATION_SCRIPT=${CI_ENVIRONMENT_INITIALIZATION_SCRIPT:-}
export CI_ENVIRONMENT_SLUG=${CI_ENVIRONMENT_SLUG:-ci-environment-slug-not-set}
export CI_ENVIRONMENT_SHORT_SLUG=${CI_ENVIRONMENT_SHORT_SLUG:-${CI_ENVIRONMENT_SLUG:(-3)}} # ^envname-[a-zA-Z0-9]{6}$$
export CI_ENVIRONMENT_DOMAIN=${CI_ENVIRONMENT_DOMAIN:-${CI_ENVIRONMENT_SHORT_SLUG}.r.${CI_BASE_DOMAIN}}
export CI_JOB_TOKEN=${CI_JOB_TOKEN:-}
export CI_PROJECT_PATH=${CI_PROJECT_PATH:-ci-project-path-not-set}
export CI_REGISTRY=${CI_REGISTRY:-ci-registry-not-set.fastplatform.eu}

export BAZEL_HOME=${HOME}/.cache/bazel
export DEFAULT_BAZEL_CACHE_ARCHIVE_NAME=bazel-external-cache.tar

export KUBE_NAMESPACE=${KUBE_NAMESPACE:-$CI_ENVIRONMENT_SLUG} # KUBE_NAMESPACE is a GitLab env variable
export NAMESPACE=${NAMESPACE:-$KUBE_NAMESPACE}
export DOCKER_REGISTRY=${DOCKER_REGISTRY:=$CI_REGISTRY}
export BAZEL_DISK_CACHE_DIR=/tmp/gitlab/cache/disk/$CI_PROJECT_PATH/$CI_COMMIT_REF_SLUG

export PGBACKREST_BACKUP_TYPE=${PGBACKREST_BACKUP_TYPE:-full}
export PGBACKREST_BACKUP_RETENTION=${PGBACKREST_BACKUP_RETENTION:-}


function force_knative_services_update() {
  sleep 2

  namespace=$1
  args=${@:2}

  timeout=5
  while [ -z "$(eval kubectl -n $namespace get ksvc $args -o jsonpath='{.items[*].metadata.name}' 2>/dev/null)" ]; do
    timeout=$((timeout-1))
    if [[ $timeout -le 0 ]]; then
      break
    fi
    sleep 1
  done
  timeout=300
  ksvcs=$(eval kubectl -n $namespace get ksvc $args -o jsonpath='{.items[*].metadata.name}')
  for k in $ksvcs; do
    timeout_latest_revision=10
    while [ -z "$(kubectl -n $namespace get ksvc $k -o jsonpath='{.status.latestCreatedRevisionName}' 2>/dev/null)" ]; do
      timeout_latest_revision=$((timeout_latest_revision-1))
      if [[ $timeout_latest_revision -le 0 ]]; then
        echo "[+] No revision found for $k service, timeout !"
        return 1
      fi
      sleep 1;
    done
    latest_revision=$(kubectl -n $namespace get ksvc $k -o jsonpath='{.status.latestCreatedRevisionName}')
    echo -n "[+] waiting latest $latest_revision revision of $k service to be ready ... "
    while ! kubectl wait -n $namespace pod --selector serving.knative.dev/revision=$latest_revision --for condition=Ready >/dev/null 2>&1; do
      timeout=$((timeout-1))
      if [[ $timeout -le 0 ]]; then
        echo "[+] timeout !"
        return 1
      fi
      sleep 1; 
    done
    echo OK

    # Delete old revision(s) manually as a workaround of https://github.com/knative/serving/issues/2720 
    kubectl -n $namespace get revision \
      --selector serving.knative.dev/service=$k \
      --field-selector metadata.name!=$latest_revision \
      -o yaml 2>/dev/null | kubectl delete -f - >/dev/null 2>&1
  done
}

function knative_service_exec() {
  namespace=$1
  service=$2
  command=${@:3}

  latest_revision=$(kubectl -n $namespace get ksvc $service -o jsonpath='{.status.latestCreatedRevisionName}')
  kubectl -n $namespace exec \
    $(kubectl -n $namespace get pod \
      --selector serving.knative.dev/revision=$latest_revision \
      -o=jsonpath='{.items[].metadata.name}') -c user-container -- \
    sh -c "$command"
}

function wait_job(){
  namespace=$1
  job_name=$2
  timeout=$3

  while ! kubectl wait -n $namespace jobs $job_name --for condition=Complete >/dev/null 2>&1; do
  sleep 1;
  timeout=$((timeout-1))
  if [[ $timeout -le 0 ]]; then
    echo "[+] timeout !"
    return 1
  fi
  done
}

function wait_pods(){
  namespace=$1
  selectors=${@:2}

  timeout=300
  while ! kubectl wait -n $namespace pods --selector "${selectors}" --for condition=Ready >/dev/null 2>&1; do
  sleep 1;
  timeout=$((timeout-1))
  if [[ $timeout -le 0 ]]; then
    echo "[+] timeout !"
    return 1
  fi
  done
}

function create_or_replace(){
  args=${@:1}

  kubectl create $args || kubectl create $args --dry-run -o yaml | kubectl replace -f -
}

function generate_review_kubeconfig(){
  namespace=$1
  kubeconfig_filename=${2:-"kubeconfig"}
  
  kubectl -n $namespace create serviceaccount gitlab-review-dev -o yaml --dry-run | kubectl apply -f - 
  kubectl -n $namespace create rolebinding gitlab-review-dev \
    --clusterrole admin \
    --serviceaccount $namespace:gitlab-review-dev \
    -o yaml --dry-run | kubectl apply -f -

  token=$(kubectl -n $namespace get secret \
    $(kubectl -n $namespace get sa gitlab-review-dev -o=jsonpath='{.secrets[0].name}') \
    -o=jsonpath={.data.token} | base64 --decode)

  kubectl config set-credentials gitlab-review-dev --token=$token
  kubectl config set-context --current --user=gitlab-review-dev --namespace=$namespace

  kubectl config view --flatten --minify > $kubeconfig_filename
}

function check_pgcluster_replicas(){
  namespace=$1
  pgo_namespace=$2
  pgcluster=$3

  pgo_client_pod=$(kubectl -n $pgo_namespace get pod -l name=pgo-client -o NAME 2>/dev/null | sed 's,.*/,,g')
  if [ -z "$pgo_client_pod" ]; then
      echo "No 'pgo-client' pod in pgo_namespace=${pgo_namespace}"
      return 1
  fi

  desired_replica_count=$(kubectl -n $namespace get pgcluster $pgcluster -o=jsonpath='{.spec.replicas}')
  replicas=($(kubectl -n $namespace get pod -l "pg-cluster=$pgcluster,name in ($pgcluster,$pgcluster-replica),role!=master" -o NAME))
  replica_count=${#replicas[@]}
  diff_replica_count=$(($desired_replica_count - $replica_count))
  if [[ $diff_replica_count == 0 ]]; then
    echo "[+] pgcluster=$pgcluster has desired_replica_count(=$desired_replica_count) and replica_count(=$replica_count) that match !"
    return
  fi
  echo "[+] pgcluster=$pgcluster has desired_replica_count(=$desired_replica_count) and replica_count(=$replica_count) that don't match!"
  echo -n "[+] converging pgcluster=$pgcluster to the desired replica count ... "
  if [[ $diff_replica_count -gt 0 ]]; then
    kubectl -n $pgo_namespace exec -t $pgo_client_pod -- pgo scale $pgcluster -n $namespace --replica-count=$diff_replica_count --no-prompt &> /dev/null
  else
    replica_deployments=($(\
      kubectl -n $namespace get pod \
      -l "pg-cluster=$pgcluster,name in ($pgcluster,$pgcluster-replica),role!=master" \
      -o=jsonpath='{.items[*].metadata.labels.deployment-name}'))
    for replica_deployment in ${replica_deployments[@]:0:$(($diff_replica_count * -1))}; do
      kubectl -n $pgo_namespace exec -t $pgo_client_pod -- pgo scaledown $pgcluster -n $namespace --target $replica_deployment --no-prompt &> /dev/null || true
    done
  fi
  echo OK
}