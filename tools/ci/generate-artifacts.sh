#!/bin/bash

set -o errexit
set -o nounset
set -o pipefail

source $(dirname $0)/common.sh

bazel build //:images.MANIFEST
cp $(bazel info bazel-genfiles)/images.MANIFEST .

# Encrypt sensitive files
git crypt lock --force
bazel build //:release_kustomize_manifest.tar.gz
cp $(bazel info bazel-genfiles)/release_kustomize_manifest.tar.gz .
