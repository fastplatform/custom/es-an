
### Structure of 

```json
{
    …,
    "titular": {…}, --> información titular de la explotación
    "unidadesProduccion": [ --> listado de unidades de producción de la explotación
        {
            …,
            "aprovechamientos": [ --> listado de aprovechamientos de una unidad de producción
                {
                    …,
                    "nivelesConveniencia": [ --> listado de niveles geométricos de un aprovechamiento
                        {
                            …,
                            "geometria": {...} --> información geométrica del nivel
                        }
                    ]
                },
                {
                    …
                }
            ]
        },
        {
            …
        },
    ]
}
```