# HELP
# This will output the help for each task
# thanks to https://marmelab.com/blog/2016/02/29/auto-documented-makefile.html
.PHONY: help

help: ## This help.
	@awk 'BEGIN {FS = ":.*?## "} /^[a-zA-Z_-]+:.*?## / {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}' $(MAKEFILE_LIST)

.DEFAULT_GOAL := help

SERVICE_HOST=0.0.0.0
SERVICE_PORT=7777

include secrets.env
export $(shell sed 's/=.*//' secrets.env)
include service.env
export $(shell sed 's/=.*//' service.env)

.PHONY: start
start: ## Start the service locally on port 7777
	uvicorn \
	app.main:app \
	--host=$(SERVICE_HOST) \
	--port=$(SERVICE_PORT) \
	--reload \
	--log-config logging-dev.json

start-tracing: ## Start the Jaeger tracing service
	docker run --rm --name tracing-fastplatform \
		-p 5775:5775/udp \
		-p 16686:16686 \
		-d docker.io/jaegertracing/all-in-one 

stop-tracing: ## Stop the Jaeger tracing service
	-docker stop tracing-fastplatform

bazel-run: ## 
	bazel run .
