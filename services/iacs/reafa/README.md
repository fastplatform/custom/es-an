# IACS/REAFA: Connector for Andalucia REAFA Farm Registry APIs

> **Note:** work in progress

This service is a connector to access farm registry data on the REAFA APIs and serve these capabilities as GraphQL mutations

It exposes 3 mutations:

- SyncHoldings: request the holdings that are accessible to the user (in the REAFA registry) and create/update the corresponding Holdings and UserRelatedParties in the FaST database
- SyncHolding: query the lines of the CAP submission of a given holding, and convert those to HoldingCampaign, Plots and Site objects in the FaST database.
- InsertDemoHolding: create a demo holding for the user (where the user is the only UserRelatedParty), based on a fixed description of site/geometry available in the `demo/sites.json` file of this repository.

### Environment variables

The environment variables used by the service are listed in the [](app/settings.py) configuration file. Default development values for some of the variables can be found in the following files:

In `service.env`:
- `API_GATEWAY_FASTPLATFORM_URL`: URL of the FaST `fastplatform` API gateway
- `API_GATEWAY_EXTERNAL_URL`: URL of the FaST `external` API gateway
- `REAFA_ACCESS_TOKEN_URL`: URL of the REAFA OAuth2 token request endpoint (defaults to `https://ws142.juntadeandalucia.es/agriculturaypesca/amgwint/oauth2/token`)
- `REAFA_API_MANAGER_URL`: URL of the REAFA APIs (defaults to `https://ws142.juntadeandalucia.es/agriculturaypesca/amgwint/reafa_fast/1.0`)

In `secret.env` (encrypted):
- `API_GATEWAY_SERVICE_KEY`: secret key of the FaST API gateways service account
- `REAFA_CONSUMER_KEY`: consumer key for the REAFA APIs
- `REAFA_CONSUMER_SECRET`: consumer secret for the REAFA APIs

## Mapping REAFA & DAT to FaST data model

> Note: work in progress

```plantuml
package "DAT Authentication" as dat <<Rectangle>> {
    class DATLoginToken {
        nif
        rol
    }

}
package "REAFA & WFS APIs" as apis <<Rectangle>> {
    class getExplotacionByNifResponse {
        provincia 
    }
    class UnidadProduccion {
        
    }
    class Aprovechamiento {
        
    }
    class NivelConveniencia {
        
    }
    getExplotacionByNifResponse "1" *-- "0..*" UnidadProduccion
    UnidadProduccion "1" *-- "0..*" Aprovechamiento
    Aprovechamiento "1" *-- "0..*" NivelConveniencia

    class getPlanificacionGeometriaResponse {
        cultivo__producto
        cultivo__variedad
        geom__codigoUnidadProduccion
        geom__codigoNivel4
    }
    class WFSFeatureResponse {

    }

}

package "Context" as context <<Rectangle>> {
    class CurrentUser {
        username
    }
}

package "FaST Data Model" as fast_data_model <<Rectangle>> {
    class Holding {
        id
        authority_id
        name
    }
    class Campaign
    class UserRelatedParty {
        user : User
        role
    }
    class HoldingCampaign
    class Site {
        authority_id
        name
    }
    class Plot {
        authority_id
        name
        geometry
    }
    class Address {
        administrative_unit_level_1
        administrative_unit_level_2
        administrative_unit_level_3
        administrative_unit_level_4
    }
    class PlotPlantVariety {
        irrigation_method
        percentage
        plant_variety : PlantVariety
    }
    
    Holding "1" *-- "0..*" HoldingCampaign
    Campaign "1" *-- "0..*" HoldingCampaign
    HoldingCampaign "1" *-- "0..*" Site
    Site "1" *-- "0..*" Plot
    Plot "1" *-- "1" Address
    Plot "1" *-- "0..*" PlotPlantVariety
    Holding "1" *-- "0..*" UserRelatedParty
}

CurrentUser::username -[#blue]-> UserRelatedParty::user

AccessGrantsResponse::registrikood -[#blue]-> Holding::id : if company
AccessGrantsResponse::isikukood -[#blue]-> Holding::id : if person
AccessGrantsResponse::juriidilineNimi -[#blue]-> Holding::name : if company
AccessGrantsResponse::eesnimi -[#blue]-> Holding::name : if person
AccessGrantsResponse::perekonnanimi -[#blue]-> Holding::name : if person

ApplicationFieldsResponse::kultuuriKood -[#blue]-> PlotPlantVariety::plant_variety
ApplicationFieldsResponse::maakond -[#blue]-> Address::administrative_unit_level_2
ApplicationFieldsResponse::massiiviNr -[#blue]-> Plot::name
ApplicationFieldsResponse::polluNr -[#blue]-> Plot::name
ApplicationFieldsResponse::maaAlaNr -[#blue]-> Plot::name
ApplicationFieldsResponse::polluId -[#blue]-> Plot::authority_id
ApplicationFieldsResponse::geomeetria -[#blue]-> Plot::geometry

Holding::id -[#blue]-> Site::authority_id
Holding::name -[#blue]-> Site::name
```

## Setting up for Development

Create a virtualenv, activate it and install dependencies:
```bash
virtualenv .venv
source .venv/bin/activate
pip install -r requirements.txt
```

Start the GraphQL server on port 9999:
```bash
make start
```
The server is then ready to receive queries at http://localhost:9999/graphql, with live reload enabled.

### Bazel

Bazel targets can be used to run the service locally:

```bash
bazel run .
```
or
```bash
bazel run //services/meteorology/weather
```

## Debug and Problem Determination

[TODO]

## Tests

Test GraphQL queries are included in the `/tests` folder.