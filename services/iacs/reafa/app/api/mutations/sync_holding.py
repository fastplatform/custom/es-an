import logging
import json
from pprint import pprint

import requests
import graphene
from gql import gql
from graphql import GraphQLError

from opentelemetry import trace

from shapely import wkt, ops
import pyproj
from shapely.geometry.multipolygon import MultiPolygon

from app.api.lib.reafa import reafa_client
from app.api.lib.gis import add_crs

from app.settings import config

# Log
logger = logging.getLogger(__name__)

# OpenTelemetry
def tracer():
    return trace.get_tracer(__name__)


class SyncHolding(graphene.Mutation):
    class Arguments:
        holding_id = graphene.String(required=True)
        campaign_id = graphene.Int(required=True)
        iacs_year = graphene.Int(required=False)

    plot_ids = graphene.List(graphene.String)

    async def mutate(self, info, holding_id, campaign_id, iacs_year=None):

        request = info.context["request"]
        client = request.state.graphql_clients["fastplatform"]

        # Retrieve info about the holding we are supposed to gather
        # geometries for
        with tracer().start_as_current_span("query_holding_info"):
            logger.debug("query_holding_info")

            query = (config.API_DIR / "graphql/query_holding_by_id.graphql").read_text()
            variables = {"holding_id": holding_id}

            # Impersonate the user
            extra_args = {
                "headers": {
                    "Authorization": f"Service {config.API_GATEWAY_SERVICE_KEY}",
                    "X-Hasura-User-Id": request.headers["X-Hasura-User-Id"],
                    "X-Hasura-Role": request.headers["X-Hasura-Role"],
                }
            }

            response = await client.execute(
                gql(query), variables, extra_args=extra_args
            )
            # Holding does not exist or the requesting user is not a
            # related party to the holding and hence is not allowed to make any
            # edit to the holding
            if not response["holding"]:
                raise GraphQLError("HOLDING_DOES_NOT_EXIST")

            holding_name = response["holding"][0]["name"]

        # Send request to REAFA endpoint for the farm structure
        with tracer().start_as_current_span("query_reafa_holding_by_nif"):
            logger.debug("query_reafa_holding_by_nif")

            try:
                data_by_nif = await reafa_client.get_explotacion_by_nif(holding_id)
                logger.debug(
                    f'received holding localizador={data_by_nif["localizador"]}'
                )
            except Exception as e:
                logger.error(str(e))
                raise GraphQLError("IACS_REMOTE_ERROR")

            if data_by_nif is None:
                logger.error(f"no data received for holding {holding_id}")
                raise GraphQLError("HOLDING_DOES_NOT_EXIST")

        with tracer().start_as_current_span("process_reafa_holding"):
            logger.debug("process_reafa_holding")
            # The REAFA data model is more complex than the one of FaST, and contains
            # information that is not necessary for FaST. The mapping from REAFA to
            # FaST is therefore as follows:
            # - single Site for all the plots
            # - the Plots correspond to the "nivel 4" of REAFA
            # - the unidad de produccion level does not appear in the hierarchy and
            #   are converted to PlotGroups

            # Extract the address of the holding
            address = {
                "administrative_unit_level_1_id": "ES",
                "administrative_unit_level_2_id": "ES-01",  # Andalucia
            }
            try:
                address["administrative_unit_level_3_id"] = (
                    "ES-01-" + data_by_nif["provincia"]["codigo"]
                )
            except:
                address["administrative_unit_level_3_id"] = None

            # Geometries that will need to be queried to the WFS layer
            geometry_ids_to_query = []

            # Objects we will later upsert
            plot_groups = {}
            site = {
                "authority_id": holding_id,
                "name": holding_name,
                "address": address,
                "plots": {},
            }

            # Process each unidad de produccion
            # We will create a plot_group for each unidad de produccion
            unidades_de_produccion = data_by_nif.get("unidadesProduccion", [])
            logger.debug(
                f"explotacion has {len(unidades_de_produccion)} unidades de produccion"
            )

            for unidad_de_produccion in unidades_de_produccion:

                codigo_unidad_de_produccion = unidad_de_produccion[
                    "codigoUnidadProduccion"
                ]
                denominacion = unidad_de_produccion["denominacion"]
                description = (
                    unidad_de_produccion["tipoUnidadProduccion"]["descripcion"] + "\n"
                )
                description += unidad_de_produccion["tipoProduccion"]["descripcion"]

                plot_group = {
                    "authority_id": codigo_unidad_de_produccion,
                    "name": denominacion,
                    "description": description,
                }

                plot_groups[codigo_unidad_de_produccion] = plot_group

                # Go through the aprovechamiento and nivel de conveniencia levels
                # flatten all to a single level
                aprovechamientos = unidad_de_produccion.get("aprovechamientos", [])
                logger.debug(
                    f"unidad de produccion {codigo_unidad_de_produccion} has {len(aprovechamientos)} aprovechamientos"
                )

                for aprovechamiento in aprovechamientos:

                    niveles_de_conveniencia = aprovechamiento.get(
                        "nivelesConveniencia", []
                    )
                    logger.debug(
                        f"aprovechamiento has {len(niveles_de_conveniencia)} niveles de conveniencia"
                    )

                    for nivel_de_conveniencia in niveles_de_conveniencia:
                        geometry_id = nivel_de_conveniencia["id"]

                        # Concatenate the ID of the unidad de produccion and the ID of the
                        # nivel de conveniencia to make a the plot authority ID
                        plot_authority_id = (
                            codigo_unidad_de_produccion
                            + "_"
                            + nivel_de_conveniencia["codigoNivel"]
                        )

                        plot_name = (
                            unidad_de_produccion["denominacion"]
                            + " / "
                            + nivel_de_conveniencia["denominacion"]
                        )

                        geometry_ids_to_query += [geometry_id]

                        site["plots"][plot_authority_id] = {
                            "authority_id": plot_authority_id,
                            "name": plot_name,
                            "address": address,
                            "geometry_id": geometry_id,
                            "plot_authority_id": plot_authority_id,
                            "plot_group": plot_group,
                            "plant_variety_ids": {},
                        }

        # Send request to REAFA endpoint for the farm geometries and crops
        with tracer().start_as_current_span("query_reafa_planificacion_geometrias"):
            logger.debug("query_reafa_planificacion_geometrias")

            try:
                data_planificacion = await reafa_client.get_planificacion_geometria(
                    data_by_nif["localizador"]
                )
            except:
                raise GraphQLError("IACS_REMOTE_ERROR")

            for planificacion in data_planificacion:
                codigo_unidad_de_produccion = planificacion["geom"][
                    "codigoUnidadProduccion"
                ]
                codigo_nivel_4 = planificacion["geom"]["codigoNivel4"]
                plot_authority_id = codigo_unidad_de_produccion + "_" + codigo_nivel_4

                # Extract the producto / variedad and build the plant_variety_id
                # as per FaST for es-an
                cultivo = planificacion.get("cultivo", None)
                if cultivo is None:
                    continue

                producto = cultivo.get("producto", {}).get("codigo", None)
                if producto is None:
                    continue

                variedad = cultivo.get("variedad", {}).get("codigo", None)
                if variedad is None or variedad == "":
                    variedad = "0"

                plant_variety_id = producto + "." + variedad

                site["plots"][plot_authority_id]["plant_variety_ids"][
                    plant_variety_id
                ] = True

        # Send request to REAFA WFS for the plot geometries
        with tracer().start_as_current_span("query_reafa_wfs_polygons"):
            logger.debug("query_reafa_wfs_polygons")

            try:
                geometries = await reafa_client.get_wfs_geometries(
                    geometry_ids_to_query
                )
            except:
                raise GraphQLError("IACS_REMOTE_ERROR")

            print(sorted([geom.area for geom in geometries.values()]))

            # Now deduplicate polygons
            # In the REAFA data model, the polygons can be repeated for each usage (aprovechamiento) ;
            # In FaST, it is the reverse, each polygon/plot is unique and can be attached to multiple
            # usages (plant varieties)
            # The below calculation is O(n2) so not optimal, would deserve some hash-based
            # optimization to run faster
            deduplicated_geometries = {}
            for geometry_id_1, geometry_1 in geometries.items():
                is_duplicate_of = None
                for geometry_id_2, obj_2 in deduplicated_geometries.items():
                    # We are using the "equals" of Shapely, which ignores the order of points
                    # We could also have used the equals_exact method with allows comparing with a spatial
                    # tolerance parameter
                    if geometry_1.equals(obj_2["geometry"]):
                        logger.debug(f'Duplicate polygons found: {geometry_id_1} == {geometry_id_2}')
                        is_duplicate_of = geometry_id_2
                        break
                if is_duplicate_of is None:
                    deduplicated_geometries[geometry_id_1] = {
                        "geometry": geometry_1,
                        "duplicate_ids": [],
                    }
                else:
                    deduplicated_geometries[is_duplicate_of]["duplicate_ids"] += [
                        geometry_id_1
                    ]

            # Join with the site/plot data
            for plot_authority_id, plot in site["plots"].items():

                # If we received no geometry for this plot, then just ignore it as FaST cannot work without
                # the geometry of a plot
                if plot["geometry_id"] not in geometries:
                    logger.error(
                        f"Plot {plot['id_nivel_de_conveniencia']} not returned by WFS."
                    )
                    del site["plots"][plot_authority_id]

                if plot["geometry_id"] not in deduplicated_geometries:
                    # It's a duplicate so will be picked up in the next for loop
                    continue
                else:
                    site["plots"][plot_authority_id][
                        "geometry"
                    ] = deduplicated_geometries[plot["geometry_id"]]["geometry"]

                for duplicate_geometry_id in deduplicated_geometries[
                    plot["geometry_id"]
                ]["duplicate_ids"]:
                    # Find the plot_authority_id coresponding to this duplicate geometry
                    duplicate_plot_authority_id = [
                        p["authority_id"]
                        for p in site["plots"]
                        if p["geometry_id"] == duplicate_geometry_id
                    ][0]
                    # Assign all the plant varieties of this duplicate plot to the current plot
                    for plant_variety_id in site["plots"][duplicate_plot_authority_id][
                        "plant_variety_ids"
                    ].keys():
                        site["plots"][plot_authority_id]["plant_variety_ids"][
                            plant_variety_id
                        ] = True
                    # Remove the duplicate plot from the site
                    del site["plots"][duplicate_plot_authority_id]

        # Build the upsert query into the database
        with tracer().start_as_current_span("build_upsert_holding_campaigns"):
            logger.debug("build_upsert_holding_campaigns")

            holding_campaigns = [
                {
                    "campaign_id": campaign_id,
                    "holding_id": holding_id,
                    "plot_groups": {
                        "data": [plot_group for plot_group in plot_groups.values()],
                        "on_conflict": {
                            "constraint": "plot_group_authority_id_holding_campaign_id_unique",
                            "update_columns": ["authority_id", "name", "description"],
                        },
                    },
                    "sites": {
                        "data": [
                            {
                                "authority_id": site["authority_id"],
                                "name": site["name"],
                                "address": {
                                    "data": plot["address"],
                                    "on_conflict": {
                                        "constraint": "address_pkey",
                                        "update_columns": [
                                            "administrative_unit_level_1_id",
                                            "administrative_unit_level_2_id",
                                            "administrative_unit_level_3_id",
                                        ],
                                    },
                                },
                                "plots": {
                                    "data": [
                                        {
                                            "authority_id": plot["authority_id"],
                                            "name": plot["name"],
                                            "geometry": add_crs(
                                                plot["geometry"].__geo_interface__,
                                                config.EPSG_SRID_ETRS89,
                                            ),
                                            "plot_plant_varieties": {
                                                "data": [
                                                    {"plant_variety_id": pvi}
                                                    for pvi in plot[
                                                        "plant_variety_ids"
                                                    ].keys()
                                                ],
                                                "on_conflict": {
                                                    "constraint": "plot_plant_variety_plot_id_plant_variety_id_unique",
                                                    "update_columns": [
                                                        "plot_id",
                                                        "plant_variety_id",
                                                    ],
                                                },
                                            },
                                            "address": {
                                                "data": plot["address"],
                                                "on_conflict": {
                                                    "constraint": "address_pkey",
                                                    "update_columns": [
                                                        "administrative_unit_level_1_id",
                                                        "administrative_unit_level_2_id",
                                                        "administrative_unit_level_3_id",
                                                    ],
                                                },
                                            },
                                        }
                                        for plot in site["plots"].values()
                                    ],
                                    "on_conflict": {
                                        "constraint": "plot_authority_id_site_id_unique",
                                        "update_columns": [
                                            "authority_id",
                                            "site_id",
                                            "name",
                                            "geometry",
                                        ],
                                    },
                                },
                            }
                        ],
                        "on_conflict": {
                            "constraint": "site_authority_id_holding_campaign_id_unique",
                            "update_columns": ["name"],
                        },
                    },
                }
            ]

        # Write out the objects, for debug
        # pprint(objects, indent=2)
        with open("holding_campaigns.json", "w") as f:
            f.write(json.dumps(holding_campaigns, indent=2))

        with tracer().start_as_current_span("insert_holding_campaign"):
            logger.debug("insert_holding_campaign")

            mutation = (
                config.API_DIR / "graphql/mutation_insert_holding_campaigns.graphql"
            ).read_text()
            variables = {"holding_campaigns": holding_campaigns}

            response = await client.execute(
                gql(mutation),
                variables,
            )

        with tracer().start_as_current_span("build_upsert_plot_plot_groups"):
            logger.debug("build_upsert_plot_plot_groups")

            # We need the insert the plot-plot_group relationships in a second query
            # (we could not do it in the first one)
            plot_groups_as_inserted = {
                plot_group["authority_id"]: plot_group["id"]
                for plot_group in response["insert_holding_campaign"]["returning"][0][
                    "plot_groups"
                ]
            }

            plots_as_inserted = response["insert_holding_campaign"]["returning"][0][
                "sites"
            ][0]["plots"]

            plot_plot_groups = [
                {
                    "plotgroup_id": plot_groups_as_inserted[
                        site["plots"][plot["authority_id"]]["plot_group"][
                            "authority_id"
                        ]
                    ],
                    "plot_id": plot["id"],
                }
                for plot in plots_as_inserted
            ]

            # with open("plot_plot_groups.json", "w") as f:
            #     f.write(json.dumps(plot_plot_groups, indent=2))

        with tracer().start_as_current_span("insert_plot_plot_groups"):
            logger.debug("insert_plot_plot_groups")

            mutation = (
                config.API_DIR / "graphql/mutation_insert_plot_plot_groups.graphql"
            ).read_text()
            variables = {"plot_plot_groups": plot_plot_groups}

            response = await client.execute(gql(mutation), variables)

        return SyncHolding(
            plot_ids=sorted([plot["authority_id"] for plot in site["plots"].values()]),
        )
