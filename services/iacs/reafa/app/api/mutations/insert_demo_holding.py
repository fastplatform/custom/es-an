import logging
import json

import requests
import graphene
from gql import gql
from opentelemetry import trace

from app.settings import config

# Log
logger = logging.getLogger(__name__)

# OpenTelemetry
def tracer():
    return trace.get_tracer(__name__)


class InsertDemoHolding(graphene.Mutation):
    """Insert a demo holding for the requesting user"""

    class Arguments:
        campaign_id = graphene.Int(required=False)

    holding_id = graphene.String()

    async def mutate(self, info, campaign_id=None):

        request = info.context["request"]
        client = request.state.graphql_clients["fastplatform"]

        # This will fail if Hasura has not injected the User-Id header
        user_id = request.headers["X-Hasura-User-Id"]

        if campaign_id is None:
            # Get the current campaign
            with tracer().start_as_current_span("query_current_campaign"):
                query = (
                    config.API_DIR / "graphql/query_current_campaign.graphql"
                ).read_text()

                response = await client.execute(gql(query))
                campaign_id = response["campaign"][0]["id"]

        # Get the mutation to create the holding
        holding = (config.API_DIR / "demo/holding.json").read_text()
        holding = holding.replace("{{ campaign_id }}", str(campaign_id))
        holding = holding.replace("{{ user_id }}", user_id)
        holding = json.loads(holding)

        # Insert the demo holding for this user
        # Note that this will fail (due to foreign key constraint violation)
        # if the user does not exist
        with tracer().start_as_current_span("insert_demo_holding"):
            mutation = (
                config.API_DIR / "graphql/mutation_insert_demo_holding.graphql"
            ).read_text()
            response = await client.execute(
                gql(mutation), variable_values={"holding": holding}
            )

            return InsertDemoHolding(holding_id=response["insert_holding_one"]["id"])
