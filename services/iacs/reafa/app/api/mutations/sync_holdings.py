import logging
import json
from pprint import pprint

import graphene
from gql import gql
from graphql import GraphQLError

from opentelemetry import trace

from app.settings import config
from app.api.lib.reafa import reafa_client

# Log
logger = logging.getLogger(__name__)


# OpenTelemetry
def tracer():
    return trace.get_tracer(__name__)


# Mapping between user roles as returned by the DAT authentication token
# and the FaST related party roles
ROLE_MAPPING = {"HOLDER": "farmer", "AUTHORIZED": "advisor"}


class SyncHoldings(graphene.Mutation):
    """Mutation for synchronizing the holdings of a user

    Expects the 'X-Hasura-User-Id' header to be set by the authentication hook

    For Andalucia, the system works in reverse in the sense that the desired holding_id
    is specified by the authentication hook when the user logs in

    Returns:
        SyncHoldings: A GraphQL node containing ok, errors and the holding_ids of the
                      farms that have been created/updated for the user
    """

    class Arguments:
        pass

    holding_ids = graphene.List(graphene.String)

    async def mutate(self, info):

        request = info.context["request"]
        client = request.state.graphql_clients["fastplatform"]

        # This will intentionally fail if Hasura has not injected the X-Hasura-User-Id header
        user_id = request.headers["X-Hasura-User-Id"]

        # Get the holding_id that was specified by DAT whent he user logged into FaST
        # We expect to find there an object {"nif": "xxxx", "rol": "HOLDER or AUTHORIZED"}
        # --> the nif we find there is supposed to be the NIF of the farm we want to connect to
        # not the the nif of the current user. Because we find a NIF there, it *means* that
        # at some point the user logged in through DAT using this NIF and therefore it is deemed
        # sufficient to assume that the user has the authorization to see this farm.
        with tracer().start_as_current_span("query_user_additional_data"):
            logger.debug("query_user_additional_data")

            query = (
                config.API_DIR / "graphql/query_user_additional_data.graphql"
            ).read_text()
            variables = {"user_id": user_id}

            # Impersonate the user and
            extra_args = {
                "headers": {
                    "Authorization": f"Service {config.API_GATEWAY_SERVICE_KEY}",
                    "X-Hasura-User-Id": request.headers["X-Hasura-User-Id"],
                    "X-Hasura-Role": request.headers["X-Hasura-Role"],
                }
            }

            response = await client.execute(
                gql(query),
                variables,
                extra_args=extra_args,
            )

            holdings_to_insert = []

            try:
                additional_data = response["private_user_info"][0]["additional_data"]
                dat_login_data = additional_data["authentication"]["dat"]

                holdings_to_insert += [
                    {"nif": dat_login_data["nif"], "rol": dat_login_data["rol"]}
                ]
                logger.debug(f'found nif={dat_login_data["nif"]} and rol={dat_login_data["rol"]}')
            except:
                # If there is nothing suitable in the "additional_data" of the user,
                # just pass
                logger.debug(
                    "the user does not have any DAT login info in his/her additional_data"
                )
                pass

        # We will also try to see if the user has a holding under his own NIF / user_id
        holdings_to_insert += [{"nif": user_id, "rol": "HOLDER"}]

        # Now check that these holdings do exist in REAFA
        with tracer().start_as_current_span("query_reafa_holdings_exist"):
            logger.debug("query_reafa_holdings_exist")

            for hi in holdings_to_insert:

                try:
                    data = await reafa_client.get_explotacion_by_nif(hi["nif"])
                except:
                    raise GraphQLError("IACS_REMOTE_ERROR")

                logger.debug(f'received holding localizador={data["localizador"]}')

                if data is not None:
                    hi["holding_id"] = hi["nif"]
                    hi["authority_id"] = data["localizador"]

                    # For the holding name, use the razon of the titular (legal name of the company)
                    # or, if not present, the full name, or the NIF
                    hi["name"] = data["titular"]["razon"]
                    if hi["name"] is None or not hi["name"]:
                        hi["name"] = data["titular"]["nombreCompleto"]
                    if hi["name"] is None or not hi["name"]:
                        hi["name"] = data["localizador"]

        # Convert items tp upsert statements
        objects = [
            {
                "id": hi["holding_id"],
                # "authority_id": hi["authority_id"],
                "name": hi["name"],
                "user_related_parties": {
                    "data": [
                        {
                            "role": ROLE_MAPPING.get(hi["rol"], "farmer"),
                            "user_id": user_id,
                        }
                    ],
                    "on_conflict": {
                        "constraint": "user_related_party_holding_id_user_id_unique",
                        "update_columns": ["role"],
                    },
                },
            }
            for hi in holdings_to_insert
            if "holding_id" in hi
        ]

        # Insert the holdings and set this user as a related party
        # Note that this will fail (due to foreign key constraint violation)
        # if the user does not exist
        with tracer().start_as_current_span("insert_holdings"):
            logger.debug("insert_holdings")

            mutation = (
                config.API_DIR / "graphql/mutation_insert_holdings.graphql"
            ).read_text()
            response = await client.execute(
                gql(mutation),
                {"objects": objects},
            )

            return SyncHoldings(holding_ids=sorted([o["id"] for o in objects]))
