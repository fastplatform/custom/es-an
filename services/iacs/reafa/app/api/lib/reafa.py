import os
import json
import logging

import requests

import httpx
from shapely.geometry import shape, MultiPolygon, Polygon

from app.settings import config
from app.api.lib.gis import Projection


logger = logging.getLogger(__name__)


class ReafaClient:
    def __init__(self):
        self.access_token = None
        self.http_client = None

    def create_http_client(self):
        self.http_client = httpx.AsyncClient()

    async def close_http_client(self):
        await self.http_client.aclose()

    async def refresh_access_token(self):
        """Refresh our OAuth token"""
        response = await self.http_client.post(
            config.REAFA_ACCESS_TOKEN_URL,
            data={
                "grant_type": "client_credentials",
                "client_id": config.REAFA_CONSUMER_KEY,
                "client_secret": config.REAFA_CONSUMER_SECRET,
            },
        )

        response.raise_for_status()

        self.access_token = response.json()["access_token"]
        logging.debug(self.access_token)
        logger.info(f"Refreshed token successfully")

    @property
    def headers(self):
        return {
            "Authorization": f"Bearer {self.access_token}",
            "Accept": "application/json",
            "Host": "ws142.juntadeandalucia.es"
        }

    async def get_explotacion_by_nif(self, nif):
        """Request the definition of an explotacion from REAFA

        See REAFA doc about the getExplotacionByNif endpoint for details.

        Args:
            nif (str): NIF of the user (= user_id in FaST)
        """
        url = config.REAFA_API_MANAGER_URL + "gestionExplotacion/getExplotacionByNif"

        response = await self.http_client.get(
            url,
            params={"nif": nif},
            headers=self.headers,
            timeout=config.REAFA_API_MANAGER_TIMEOUT,
        )

        if response.status_code == httpx.StatusCode.UNAUTHORIZED:
            await self.refresh_access_token()
            response = await self.http_client.get(
                url,
                params={"nif": nif},
                headers=self.headers,
                timeout=config.REAFA_API_MANAGER_TIMEOUT,
            )

        response.raise_for_status()
        data = response.json()

        if data.get("errores"):
            logger.error(f"Query failed with error: {data.get('errores')}")
            raise Exception("IACS_DATA_ERROR")

        if not data.get("resultado", None):
            logger.error("Query return no result")
            raise Exception("IACS_DATA_ERROR")

        return data.get("objeto")

    async def get_planificacion_geometria(self, localizador):
        """Request the planification (crops) of an explotacion from REAFA

        See REAFA doc about the getPlanificacionGeometria endpoint for details.

        Args:
            localizador (str): REAFA identifier of the explotacion (= holding_id in FaST)
        """
        url = (
            config.REAFA_API_MANAGER_URL
            + "gestionExplotacion/getPlanificacionGeometria"
        )

        response = await self.http_client.get(
            url,
            params={"codigo": localizador},
            headers=self.headers,
            timeout=config.REAFA_API_MANAGER_TIMEOUT,
        )

        if response.status_code == httpx.StatusCode.UNAUTHORIZED:
            await self.refresh_access_token()
            response = await self.http_client.get(
                url,
                params={"codigo": localizador},
                headers=self.headers,
                timeout=config.REAFA_API_MANAGER_TIMEOUT,
            )

        response.raise_for_status()
        data = response.json()

        if data.get("errores"):
            logger.error(f"Query failed with error: {data.get('errores')}")
            raise Exception("IACS_DATA_ERROR")

        if not data.get("resultado", None):
            logger.error("Query return no result")
            raise Exception("IACS_DATA_ERROR")

        return data.get("objeto")

    async def get_wfs_geometries(self, geometry_ids):
        """[summary]

        Args:
            geometries ([type]): [description]
        """

        geometries = {}

        for start in range(
            0, len(geometry_ids), config.REAFA_GEOMETRIAS_WFS_BATCH_SIZE
        ):
            batch_ids = geometry_ids[
                start : start + config.REAFA_GEOMETRIAS_WFS_BATCH_SIZE
            ]
            batch_ids = map(str, batch_ids)

            response = await self.http_client.get(
                config.REAFA_GEOMETRIAS_WFS_URL,
                params={
                    "service": "WFS",
                    "version": "1.0.0",
                    "request": "GetFeature",
                    "typeName": "REAFA:RAFV_NIVEL_CONVENIENCIA",
                    "CQL_FILTER": f"CD_ID IN ({', '.join(batch_ids)})",
                    "outputFormat": "application/json",
                },
                timeout=config.REAFA_GEOMETRIAS_WFS_TIMEOUT,
            )

            response.raise_for_status()
            data = response.json()

            try:
                for feature in data.get("features", []):
                    geometry = shape(feature["geometry"])
                    if isinstance(geometry, Polygon):
                        geometry = MultiPolygon([geometry])
                    geometry = Projection.etrs89_utm30n_to_etrs89(geometry)
                    geometries[feature["properties"]["CD_ID"]] = geometry
            except:
                raise Exception("IACS_DATA_ERROR")

        return geometries


reafa_client = ReafaClient()