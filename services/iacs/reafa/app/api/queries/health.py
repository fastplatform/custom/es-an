import graphene


class HealthQuery(graphene.ObjectType):

    iacs_arpea__healthz = graphene.String(default_value='ok')
