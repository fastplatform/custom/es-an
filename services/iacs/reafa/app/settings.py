import os
import sys

from pathlib import Path, PurePath

from pydantic import BaseSettings

from typing import Dict, Union

class Settings(BaseSettings):

    API_DIR: Path = PurePath(__file__).parent / "api"

    EPSG_SRID_ETRS89: str = "4258"
    EPSG_SRID_ETRS89_LAEA: str = "3035"
    EPSG_SRID_ETRS89_UTM30N: str = "25830"

    API_GATEWAY_EXTERNAL_URL: str
    API_GATEWAY_FASTPLATFORM_URL: str
    # 2 minutes, there can be some heavy farms to write to Hasura
    API_GATEWAY_TIMEOUT: int = 120
    API_GATEWAY_SERVICE_KEY: str

    OPENTELEMETRY_EXPORTER_JAEGER_SERVICE_NAME: str = "iacs-arpea"
    OPENTELEMETRY_EXPORTER_JAEGER_AGENT_HOSTNAME: str = "127.0.0.1"
    OPENTELEMETRY_EXPORTER_JAEGER_AGENT_PORT: int = 5775
    OPENTELEMETRY_SAMPLING_RATIO: float = 0.1

    REAFA_CONSUMER_KEY: str
    REAFA_CONSUMER_SECRET: str
    REAFA_API_MANAGER_URL: str
    REAFA_API_MANAGER_TIMEOUT: int
    REAFA_ACCESS_TOKEN_URL: str
    REAFA_GEOMETRIAS_WFS_URL: str
    REAFA_GEOMETRIAS_WFS_BATCH_SIZE: int
    REAFA_GEOMETRIAS_WFS_TIMEOUT: int

    IACS_DISPLAY_NAME: str = (
        "Registro de Explotaciones Agrarias y Forestales de Andalucía"
    )
    IACS_SHORT_NAME: str = "REAFA"
    IACS_WEBSITE_URL: str = "https://www.juntadeandalucia.es/organismos/agriculturaganaderiapescaydesarrollosostenible/areas/agricultura/produccion-agricola/paginas/reafa.html"

    LOG_COLORS = bool(os.getenv("LOG_COLORS", sys.stdout.isatty()))
    LOG_FORMAT = str(os.getenv("LOG_FORMAT", "uvicorn"))
    LOG_LEVEL = str(os.getenv("LOG_LEVEL", "info")).upper()
    LOGGING_CONFIG: Dict[str, Union[Dict, bool, int, str]] = {
        "version": 1,
        "disable_existing_loggers": False,
        "formatters": {
            "simple": {
                "class": "logging.Formatter",
                "format": "%(levelname)-10s %(message)s",
            },
            "uvicorn": {
                "()": "uvicorn.logging.DefaultFormatter",
                "format": "%(levelprefix)s %(message)s",
                "use_colors": LOG_COLORS,
            },
        },
        "handlers": {
            "default": {
                "class": "logging.StreamHandler",
                "formatter": LOG_FORMAT,
                "level": LOG_LEVEL,
                "stream": "ext://sys.stdout",
            }
        },
        "root": {"handlers": ["default"], "level": LOG_LEVEL},
        "loggers": {
            "fastapi": {"propagate": True},
            "uvicorn": {"propagate": True},
            "uvicorn.access": {"propagate": True},
            "uvicorn.asgi": {"propagate": True},
            "uvicorn.error": {"propagate": True},
        },
    }


config = Settings()