# idp/authentication

The idp authentication service of the module ```es-an``` is built using the [Django](https://www.djangoproject.com/) framework.
It handles authentication as an intermediary between the [DAT] (https://ws229.juntadeandalucia.es/dat/bienvenido.xhtml) (Documento de Acompañamiento al Transporte) and the FaST web application.

## Development setup

### Prerequisites

- Python 3.8+ and [`virtualenv`](https://virtualenv.pypa.io/en/latest/)
- 
- PostGIS databases need to be started

### Environment variables

The idp authentication expects the following environment variables to be set:

- `DJANGO_SECRET_KEY`: a secret key for Django cryptographic features (the same as web authentication)
- `POSTGRES_PASSWORD`: password to the main PostGIS database
- `AUTHENTICATION_REDIRECT_URL`: url to redirect user if an error occured in login process
- `DAT_JWKS_URL`: url to get keys to check dat token
- `DAT_AUDIENCE`: audience used in DAT token
- `WEB_APP_URL`: Url of the FaST web application to redirect user once the login process is done

### Setup

Create a Python virtualenv and activate it:
```
virtualenv .venv
source .venv/bin/activate
```

Install Python dependencies:
```
pip install -r requirements.txt

# Optionally install dev packages
pip install -r requirements-dev.txt
```

Start the web server:
```
make start
```

The web server is now started and run at http://localhost:8001/.