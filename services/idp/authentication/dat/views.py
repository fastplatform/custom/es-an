from authentication.utils import import_from_settings
from django.conf import settings
from django.contrib import messages
from django.contrib.auth import get_user_model, login
from django.shortcuts import redirect, render
from django.urls import reverse
from django.views.generic import View
from oidc_provider.lib.utils.token import (
    create_id_token,
    create_token,
    encode_id_token,
)
from oidc_provider.models import (
    Client,
    Code,
    Token,
)

import json
import jwt
import random
import requests
import string
import os

CURRENT_DIR = os.path.dirname(os.path.abspath(__file__))

class DatAuthenticationCallbackView(View):
    """ Once the token is send back,
     - check it through the op cyl api
     - if correct, create the user if needed and log in
     - redirect to the authentication_callback view 
    """

    http_method_names = ['get']

    def __init__(self, *args, **kwargs):
        self.UserModel = get_user_model()
        self.AUTHENTICATION_REDIRECT_URL = import_from_settings('AUTHENTICATION_REDIRECT_URL')

    def get(self, request):
        token = request.GET.get('token')

        # Check the token using the Op Dat's api to get public key
        jwks_url = settings.DAT_JWKS_URL
        jwks = requests.get(jwks_url, verify=os.path.abspath(os.path.join(CURRENT_DIR, os.pardir)) + '/my_trust_store.pem').json()

        # define public keys
        public_keys = {}
        for jwk in jwks['keys']:
            kid = jwk['kid']
            public_keys[kid] = jwt.algorithms.RSAAlgorithm.from_jwk(json.dumps(jwk))
        
        # get the needed public key
        # kid = jwt.get_unverified_header(token)['kid']
        key = public_keys['datfast']
        decode = jwt.decode(token, key, algorithms=['RS256'], audience=settings.DAT_AUDIENCE)
        # If token is validated,
        # Create the user if needed and log in 
            
        # Get or create User if needed
        user = self.get_or_create_user(decode)
        # check if user is active
        if user.is_active:
            # Store token to be reused in the future
            login(request, user)

            # Generate token
            access_token, refresh_token, id_token = self.create_token(user)

            dic = {
                'accessToken': access_token,
                'refreshToken': refresh_token,
                'tokenType': 'bearer',
                'expiresIn': 3600,
                'idToken': id_token,
                'region': 'es-an'
            }
            
            dic_list = []
            for key in dic:
                dic_list.append(key + '=' + str(dic[key]))
            query_params = '&'.join(dic_list)

        else:
            messages.error(request, error_message)
            return redirect(self.AUTHENTICATION_REDIRECT_URL)
            
        return redirect(settings.WEB_APP_URL_REGION_SELECTION + '?' + query_params)


    def create_token(self, user):
        client = Client.objects.get(name='fastplatform')
        scope = ['openid', 'offline_access', 'profile', 'email']

        access_token, refresh_token, at_hash, token = create_token(
            user=user,
            client=client,
            scope=scope)
        id_token_dic = create_id_token(
            user=user,
            aud=client.client_id,
            token=token,
            nonce=None,
            at_hash=at_hash,
            request=self.request,
            scope=scope,
        )

        token.id_token = id_token_dic
        token.save()

        id_token = encode_id_token(id_token_dic, token.client)
        return access_token, refresh_token, id_token

    def get_or_create_user(self, decode):
        """Returns a User instance if a user is found. Creates a user if not found."""
        if decode['rol'] == 'HOLDER':
            username = decode['nifHolder']
            dat_additional_data = {
                'nif': decode['nifHolder'],
                'rol': 'HOLDER'
            
            }
        elif decode['rol'] == 'AUTHORIZED':
            username = decode['nifUserLogin']
            dat_additional_data = {
                'nif': decode['nifHolder'],
                'rol': 'AUTHORIZED'
            
            }

        users = self.UserModel.objects.filter(username__iexact=username)

        user = None

        if len(users) == 1:
            # User has been found, we update its information with the latest received
            user = self.update_user(users[0], decode)
        else:
            # User has not been found, so we create one
            user = self.create_user(decode, username)

        if user.additional_data is None:
            user.additional_data = {'authentication': {'dat': dat_additional_data}}
            user.save()
        else:
            user.additional_data['authentication'] = {'dat': dat_additional_data}
            user.save()

        return user

    def create_user(self, decode, username):
        """Creates a user"""
        # For the moment, DAT do not send us first name, last name or full name
        # Get first name and last name, then define full name
        # first_name = decode['nombre']
        # last_name = decode['apellidos']
        # name = first_name + ' ' + last_name

        # return self.UserModel.objects.create_user(username=username, name=name, first_name=first_name, last_name=last_name)
        return self.UserModel.objects.create_user(username=username)

    def update_user(self, user, decode):
        """Update user's information with the decoded token"""
        # For the moment, DAT do not send us first name, last name or full name
        # first_name = decode['nombre']
        # last_name = decode['apellidos']
        # name = first_name + ' ' + last_name

        # user.first_name = first_name
        # user.last_name = last_name
        # user.name = name
        user.save()
        return user