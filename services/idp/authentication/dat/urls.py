from django.contrib.auth import views as auth_views
from django.urls import include, path
from . import views
from os import environ

urlpatterns = [
    # path('', views.DatAuthenticationView.as_view(), name='index'),
    path('callback', views.DatAuthenticationCallbackView.as_view(), name='callback'),
]